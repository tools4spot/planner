# SPOT Shiftmanager

Alloceren, plannen en misschien ooit registreren

## Ontwerp [titel]

Servicemanagement:

Procesmanagement:

- [x] Beheren procesontwerpen (architect)
    - [x]  Bepalen resultaatgebieden
- [x] Toewijzen resultaatgebieden aan een team; wanneer meerdere teams aan eenzelfde resultaatgebied werken is het raadzaam dit resultaatgebied op te splitsen per team
- [x] Inplannen shifts voor de resultaatgebieden (coördinator)
    - Veronderstelling: procesmanagement heeft zicht op (verwachte) behoefte aan inzet per resultaatgebied.
    - [ ] Procesmanagement heeft inzicht in beschikbare uren per resultaatgebied.
    - [ ] Procesmanagement heeft inzicht in verhouding gevraagde, gealloceerde, ingeplande en beschikbare uren.

Lijnmanagement:

- Managen werknemers
    - [x] CRUD werknemers
    - [ ] Beheren contracturen per werknemer (vast, flex, min/max)
    - [x] CRUD teams 
    - [x] Wekelijkse uren per teamlid
    - [ ] Beheren afwezigheid werknemers: vakanties, vrije dagen
    - [ ] Inzicht per team behoefte en beschikbaarheid uren/expertise, gebaseerd op allocatie door procesbeheer
    - [ ] Beheren beschikbaarheid teamleden: wanneer beschikbaar voor welk team
- Managen teams en teamleden
    - [x] Teamleden koppelen aan resultaatgebieden
        - [x] Lijn kan gewenst wekelijks budget per toewijzing instellen (indicatief)
    - [ ] Inplannen beschikbare werknemers op door procesmanagement gealloceerde shifts
    - Veronderstelling: de werkzaamheden voor resultaatgebied zijn belegd bij één team.
    - Veronderstelling: projectinzet van werknemers organiseren via een team: projectteam

Todo:

- [ ] authenticatie
- [ ] autorisatie, taken, rollen en scope (teams / processen) 
    - procesarchitect: processen en resultaatgebieden
    - procesmanager: toewijzen resultaatgebieden aan teams
    - bepalen behoefte per resultaatgebied per periode (maand/kwartaal)
    - procescoördinator: alloceren shifts
    - teammanager: toewijzen resultaatgebieden (wie wat)
    - teamcoördinator: inplannen shifts (wie wanneer)
- [ ] output: werkrooster voor werknemer

Out of scope:
- [ ] urenregistratie
- Vereiste / geleverde prestaties per proces en resultaatgebied
- (Verwachte) behoefte aan inzet per resultaatgebied

Questions:


