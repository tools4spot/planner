from ..models import Allocation, AllocationResponse, Assignment, AssignmentResponse, Team
from ..database import get_session
from fastapi import APIRouter, Depends, HTTPException, Query
from sqlmodel import Session, select
from datetime import datetime

router = APIRouter()


def get_team(session: Session, item_id):
    item = session.get(Team, item_id)
    if not item:
        raise HTTPException(status_code=404, detail="Team not found")
    return item

def get_allocation(session: Session, item_id):
    item = session.get(Allocation, item_id)
    if not item:
        raise HTTPException(status_code=404, detail="Allocation not found")
    return item

def get_assignment(session: Session, item_id):
    item = session.get(Assignment, item_id)
    if not item:
        raise HTTPException(status_code=404, detail="Assignment not found")
    return item



@router.get(
    "/teams/{team_id}/allocations/",
    response_model=AllocationResponse,
    tags=["allocations"],
)
def read_allocations(
    *,
    session: Session = Depends(get_session),
    team_id: int,
    offset: int = 0,
    limit: int = Query(default=100, le=100),
    week: int = Query(default=datetime.now().isocalendar()[1], ge=1, le=53),
    year: int = Query(default=datetime.now().isocalendar()[0])
):
    get_team(session, team_id)
    # TODO: relate team to allocation and add .where(Alloction.team_id == team_id)
    statement = (
        select(Allocation)
        .where(Allocation.week == week)
        .where(Allocation.year == year)
        .offset(offset)
        .limit(limit)
    )
    result = session.exec(statement).all()
    return result


@router.post("/teams/{team_id}/allocations/{allocation_id}/assignment/", response_model=AssignmentResponse)
def create_assignment(
    *, session: Session = Depends(get_session),
    team_id: int,
    allocation_id: int,
):
    get_team(session, team_id)
    get_allocation(session, allocation_id)

@router.get("/teams/{team_id}/allocations/{allocation_id}/assignment/{assignment_id}")
def read_assignment(
    *,
    session: Session = Depends(get_session),
    team_id: int,
    allocation_id: int,
    assignment_id: int
):
    get_team(session, team_id)
    get_allocation(session, allocation_id)
    get_assignment(session, assignment_id)


@router.put("/teams/{team_id}/allocations/{allocation_id}/assignment/{assignment_id}")
def update_assignment(
    *,
    session: Session = Depends(get_session),
    team_id: int,
    allocation_id: int,
    assignment_id: int
):
    get_team(session, team_id)
    get_allocation(session, allocation_id)
    get_assignment(session, assignment_id)



@router.delete(
    "/teams/{team_id}/allocations/{allocation_id}/assignment/{assignment_id}"
)
def remove_assignment(
    *,
    session: Session = Depends(get_session),
    team_id: int,
    allocation_id: int,
    assignment_id: int
):
    get_team(session, team_id)
    get_allocation(session, allocation_id)
    get_assignment(session, assignment_id)
