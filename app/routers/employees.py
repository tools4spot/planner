from ..models import Employee, EmployeeCreate, EmployeeUpdate
from ..database import engine, get_session
from fastapi import APIRouter, Depends, HTTPException, Query
from sqlmodel import Session, select

router = APIRouter()

def get_item(session: Session, item_id):
    item = session.get(Employee, item_id)
    if not item:
        raise HTTPException(status_code=404, detail="Item not found")

@router.get("/")
def read_items(
    *, session: Session=Depends(get_session),
    offset: int = 0,
    limit: int = Query(default=100, le=100)
):
    statement = select(Employee).offset(offset).limit(limit)
    result = session.exec(statement).all()
    return result

@router.post("/")
def create_item(
    *, session: Session=Depends(get_session),
    item: EmployeeCreate
):
    db_item = Employee.model_validate(item)
    session.add(db_item)
    session.commit()
    session.refresh(db_item)
    return db_item

@router.get("/{item_id}")
def read_item(
    *, session: Session=Depends(get_session),
    item_id: int
):
    item = get_item(session, item_id)
    return item

@router.put("/{item_id}")
def update_item(
    *, session: Session=Depends(get_session),
    item_id: int,
    item: EmployeeUpdate
):
    db_item = get_item(session, item_id)
    item_data = db_item.model_dump(item)
    db_item.sqlmodel_update(item_data)

    session.add(db_item)
    session.commit()
    session.refresh(db_item)
    return db_item
    

@router.delete("/{item_id}")
def remove_item(
    *, session: Session=Depends(get_session),
    item_id: int
):
    item = get_item(session, item_id)
    session.delete(item)
