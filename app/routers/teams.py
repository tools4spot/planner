from ..models import Team, TeamCreate, TeamMember, TeamMemberCreate, TeamMemberResponse, TeamMemberUpdate, TeamRead, TeamRespons, TeamUpdate
from ..database import engine, get_session
from fastapi import APIRouter, Depends, HTTPException, Query
from sqlmodel import Session, select


router = APIRouter()

def get_team(session: Session, team_member_id):
    item = session.get(Team, team_member_id)
    if not item:
        raise HTTPException(status_code=404, detail="Team not found")
    
def get_team_member(session: Session, team_member_id):
    item = session.get(TeamMember, team_member_id)
    if not item:
        raise HTTPException(status_code=404, detail="Team member not found")

# =============================================================================
# CRUD Teams
# =============================================================================

@router.post("/teams/", response_model=TeamRespons, tags=["teams"])
def create_team(*, session: Session = Depends(get_session), process: TeamCreate):
    team = Team.model_validate(process)
    session.add(team)
    session.commit()
    session.refresh(team)
    return team


@router.get("/teams/", response_model=list[TeamRead], tags=["teams"])
def read_teams(
    *,
    session: Session = Depends(get_session),
    offset: int = 0,
    limit: int = Query(default=100, le=100)
):
    teams = session.exec(select(Team).offset(offset).limit(limit)).all()
    return teams


@router.get(
    "/teams/{team_id}",
    response_model=TeamRespons,
    tags=["teams"],
)
def read_team(*, session: Session = Depends(get_session), team_id: int):
    team = session.get(Team, team_id)
    if not team:
        raise HTTPException(status_code=404, detail="Team not found")
    return team

@router.put("/teams/{team_id}", tags=["teams"])
def update_item(
    *, session: Session=Depends(get_session),
    team_id: int,
    item: TeamUpdate
):
    db_item = get_team(session, team_id)
    item_data = db_item.model_dump(item)
    db_item.sqlmodel_update(item_data)

    session.add(db_item)
    session.commit()
    session.refresh(db_item)
    return db_item
    

@router.delete("/teams/{team_id}", tags=["teams"])
def remove_item(
    *, session: Session=Depends(get_session),
    team_id: int
):
    item = get_team(session, team_id)
    session.delete(item)


# =============================================================================
# CRUD Team Members
# =============================================================================


@router.post("/teams/{team_id}/members/", tags=["team members"])
def create_member(
    *,
    session: Session = Depends(get_session),
    team_id: int,
    team_member: TeamMemberCreate
):
    db_team_member = TeamMember.validate(team_member)
    db_team_member.team_id = team_id
    session.add(db_team_member)
    session.commit()
    session.refresh(db_team_member)
    return db_team_member


@router.get(
    "/teams/{team_id}/members/", response_model=list[TeamMemberResponse], tags=["team members"]
)
def read_team_members(*, session: Session = Depends(get_session), team_id: int):
    team = session.get(Team, team_id)
    if not team:
        raise HTTPException(status_code=404, detail="Team not found")
    return team.member_links

@router.get("/teams/{team_id}/members/{team_member_id}", response_model=TeamMemberResponse, tags=["team members"])
def read_team_member(
    *, session: Session=Depends(get_session),
    team_id: int,
    team_member_id: int
):
    get_team(session, team_id)
    item = get_team_member(session, team_member_id)
    return item

@router.put("/teams/{team_id}/members/{team_member_id}", response_model=TeamMemberResponse, tags=["team members"])
def update_item(
    *, session: Session=Depends(get_session),
    team_id: int,
    team_member_id: int,
    item: TeamMemberUpdate
):
    get_team(session, team_id)
    db_item = get_team_member(session, team_member_id)
    item_data = db_item.model_dump(item)
    db_item.sqlmodel_update(item_data)

    session.add(db_item)
    session.commit()
    session.refresh(db_item)
    return db_item
    

@router.delete("/teams/{team_id}/members/{team_member_id}", tags=["team members"])
def remove_item(
    *, session: Session=Depends(get_session),
    team_id: int,
    team_member_id: int
):
    get_team(session, team_id)
    team_member = get_team_member(session, team_member_id)
    session.delete(team_member)
