from ..database import engine, get_session
from ..models import Allocation, AllocationCreate, AllocationResponse, AllocationUpdate, Process, ProcessCreate, ProcessRead, ProcessResponse, ProcessUpdate, ResultArea, ResultAreaCreate, ResultAreaRead
from fastapi import APIRouter, Depends, HTTPException, Query
from sqlmodel import Session, select
from sqlalchemy.exc import IntegrityError
from psycopg.errors import UniqueViolation
from datetime import timedelta

router = APIRouter()


def get_process(session: Session, process_id: int):
    process = session.get(Process, process_id)
    if not process:
        raise HTTPException(status_code=404, detail="Process not found")
    return process


def get_result_area(session: Session, id: int):
    result = session.get(ResultArea, id)
    if not result:
        raise HTTPException(status_code=404, detail="Step not found")
    return result


def get_allocation(session: Session, id: int):
    result = session.get(Allocation, id)
    if not result:
        raise HTTPException(status_code=404, detail="Result Area Allocation not found")
    return result


@router.post("/processes/", response_model=ProcessRead, tags=["processes"])
def create_process(*, session: Session = Depends(get_session), process: ProcessCreate):
    db_process = Process.model_validate(process)
    session.add(db_process)
    session.commit()
    session.refresh(db_process)
    return db_process


@router.get("/processes/", response_model=list[ProcessRead], tags=["processes"])
def read_processes(
    *,
    session: Session = Depends(get_session),
    offset: int = 0,
    limit: int = Query(default=100, le=100)
):
    processes = session.exec(select(Process).offset(offset).limit(limit)).all()
    return processes


@router.get(
    "/processes/{process_id}",
    response_model=ProcessResponse,
    tags=["processes"],
)
def read_process(*, session: Session = Depends(get_session), process_id: int):
    process = get_process(session, process_id)
    return process


@router.put(
    "/processes/{process_id}",
    response_model=ProcessResponse,
    tags=["processes"],
)
def update_process(
    *, session: Session = Depends(get_session), process_id: int, process: ProcessUpdate
):
    db_process = get_process(session, process_id)
    process_data = db_process.model_dump(process)
    db_process.sqlmodel_update(process_data)
    session.add(db_process)
    session.commit()
    session.refresh(db_process)
    return db_process


@router.delete(
    "/processes/{process_id}",
    response_model=ProcessResponse,
    tags=["processes"],
)
def remove_process(*, session: Session = Depends(get_session), process_id: int):
    process = get_process(session, process_id)
    session.delete(process)


# =============================================================================
# CRUD result areas (steps)
# =============================================================================
@router.get(
    "/processes/{process_id}/steps/",
    response_model=list[ResultAreaRead],
    tags=["result areas"],
)
def get_result_areas(
    *,
    session: Session = Depends(get_session),
    process_id: int,
    offset: int = 0,
    limit: int = Query(default=100, le=100)
):
    get_process(session, process_id)

    statement = (
        select(ResultArea)
        .where(ResultArea.process_id == process_id)
        .offset(offset)
        .limit(limit)
    )
    result_areas = session.exec(statement).all()
    return result_areas


@router.post(
    "/processes/{process_id}/steps/",
    response_model=ResultAreaRead,
    tags=["result areas"],
)
def create_result_area(
    *,
    session: Session = Depends(get_session),
    process_id: int,
    result_area: ResultAreaCreate
):
    process = get_process(session, process_id)

    db_result_area = ResultArea.model_validate(result_area)
    db_result_area.process_id = process_id
    session.add(db_result_area)

    try:
        session.commit()
    except IntegrityError as e:
        print(e)
        print(type(e.orig))
        if isinstance(e.orig, UniqueViolation):
            raise HTTPException(status_code=409, detail="UniqueViolation")
        else:
            print(e.orig)
            raise HTTPException(status_code=500, detail="UnknownError")

    session.refresh(db_result_area)
    return db_result_area

@router.put("/processes/{process_id}/steps/{step_id}", tags=["result areas"])
def update_result_area(*, session: Session = Depends(get_session), process_id: int):
    get_process(session, process_id)


    pass

@router.delete("/processes/{process_id}/steps/{step_id}", tags=["result areas"])
def remove_result_area(*, session: Session = Depends(get_session), process_id: int):
    pass

# =============================================================================
# Daily allocation of resources for the result areas (shifts)
# =============================================================================

@router.post(
    "/processes/{process_id}/steps/{step_id}/scheduled/",
    response_model=AllocationResponse,
    tags=["allocations"],
)
def create_result_area_allocation(
    *,
    session: Session = Depends(get_session),
    process_id: int,
    step_id: int,
    alloc: AllocationCreate
):
    process = get_process(session, process_id)
    step = get_result_area(session, step_id)

    if alloc.startTime is not None:
        alloc.endTime = alloc.startTime + timedelta(minutes=alloc.duration_minutes)

    db_alloc = Allocation.model_validate(alloc)
    db_alloc.result_area_id = step_id
    db_alloc.year = db_alloc.startTime.isocalendar()[0]
    db_alloc.week = db_alloc.startTime.isocalendar()[1]
    db_alloc.weekday = db_alloc.startTime.isocalendar()[2]
    session.add(db_alloc)

    session.commit()
    session.refresh(db_alloc)
    return db_alloc


@router.get(
    "/processes/{process_id}/steps/{step_id}/scheduled/",
    response_model=list[AllocationResponse],
    tags=["allocations"],
)
def read_result_area_allocations(
    *,
    session: Session = Depends(get_session),
    process_id: int,
    step_id: int,
    offset: int = 0,
    limit: int = Query(default=100, le=100)
):
    process = get_process(session, process_id)
    step = get_result_area(session, step_id)

    statement = select(Allocation).offset(offset).limit(limit)
    result = session.exec(statement).all()
    return result


@router.get(
    "/processes/{process_id}/steps/{step_id}/scheduled/{alloc_id}",
    response_model=AllocationResponse,
    tags=["allocations"],
)
def read_result_area_allocation(
    *,
    session: Session = Depends(get_session),
    process_id: int,
    step_id: int,
    alloc_id: int
):
    process = get_process(session, process_id)
    step = get_result_area(session, step_id)
    alloc = get_allocation(session, alloc_id)
    return alloc


@router.put(
    "/processes/{process_id}/steps/{step_id}/scheduled/{alloc_id}",
    response_model=AllocationResponse,
    tags=["allocations"],
)
def update_result_area_allocation(
    *,
    session: Session = Depends(get_session),
    process_id: int,
    step_id: int,
    alloc_id: int,
    alloc: AllocationUpdate
):
    process = get_process(session, process_id)
    step = get_result_area(session, step_id)

    if alloc.startTime is not None:
        alloc.endTime = alloc.startTime + timedelta(minutes=alloc.duration_minutes)
    
    db_alloc = get_allocation(session, alloc_id)
    alloc_data = alloc.model_dump(exclude_unset=True)
    db_alloc.sqlmodel_update(alloc_data)
    db_alloc.year = db_alloc.startTime.isocalendar()[0]
    db_alloc.week = db_alloc.startTime.isocalendar()[1]
    db_alloc.weekday = db_alloc.startTime.isocalendar()[2]
    session.add(db_alloc)
    session.commit()
    session.refresh(db_alloc)
    return db_alloc


@router.delete(
    "/processes/{process_id}/steps/{step_id}/scheduled/{alloc_id}", tags=["allocations"]
)
def remove_result_area_allocation(
    *,
    session: Session = Depends(get_session),
    process_id: int,
    step_id: int,
    alloc_id: int
):
    process = get_process(session, process_id)
    step = get_result_area(session, step_id)
    alloc = get_allocation(session, alloc_id)

    session.delete(alloc)
