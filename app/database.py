from sqlmodel import create_engine, Session, SQLModel

db_url = "postgresql+psycopg://postgres:postgres@db/postgres"

connect_args = {"check_same_thread": False}

engine = create_engine(db_url, echo=True)


def get_session():
    with Session(engine) as session:
        yield session

def create_db_and_tables():
    SQLModel.metadata.create_all(engine)

def db_drop_tables():
    SQLModel.metadata.drop_all(engine)
