from fastapi import FastAPI
from fastapi.concurrency import asynccontextmanager
from .routers import employees, processes, schedules, teams
from .database import create_db_and_tables, db_drop_tables


@asynccontextmanager
async def lifespan(app: FastAPI):
    create_db_and_tables()
    yield
    # db_drop_tables()

app = FastAPI(lifespan=lifespan)

app.include_router(processes.router)
app.include_router(teams.router)
app.include_router(employees.router, prefix="/employees", tags=["employees"])
app.include_router(schedules.router, tags=["schedules"])
