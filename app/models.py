from datetime import datetime
from typing import Optional
from .database import SQLModel
from sqlalchemy import UniqueConstraint
from sqlmodel import Field, Relationship

class AssignmentBase(SQLModel):
    pass


class Assignment(AssignmentBase, table=True):
    id: Optional[int] = Field(default=None, primary_key=True)

    allocation_id: int | None = Field(default=None, foreign_key="allocation.id", primary_key=True)
    team_member_id: int | None = Field(default=None, foreign_key="teammember.id", primary_key=True)


class AssignmentRead(AssignmentBase):
    id: int

class TeamMemberResultAreaLinkBase(SQLModel):
    hours_weekly: int | None = Field(default=None)


class TeamMemberResultAreaLink(SQLModel, table=True):
    id: Optional[int] = Field(default=None, primary_key=True)

    team_member_id: int | None = Field(
        default=None, foreign_key="teammember.id", primary_key=True
    )
    result_area_id: int | None = Field(
        default=None, foreign_key="resultarea.id", primary_key=True
    )


class TeamMember(SQLModel, table=True):
    id: Optional[int] = Field(default=None, primary_key=True)

    team_id: int | None = Field(default=None, foreign_key="team.id", primary_key=True)
    employee_id: int | None = Field(
        default=None, foreign_key="employee.id", primary_key=True
    )

    hours_weekly: int = Field(default=0)
    note: str | None = Field(default=None)

    team: "Team" = Relationship(back_populates="member_links")
    employee: "Employee" = Relationship(back_populates="team_links")

    result_areas: list["ResultArea"] = Relationship(
        back_populates="team_members", link_model=TeamMemberResultAreaLink
    )

    assignments: list["Allocation"] = Relationship(back_populates="assigned_team_members", link_model=Assignment)


class TeamMemberCreate(SQLModel):
    employee_id: int
    hours_weekly: Optional[int] = Field(default=0)


class TeamMemberRead(SQLModel):
    id: int
    hours_weekly: int


class TeamMemberUpdate(SQLModel):
    hours_weekly: int


class TeamBase(SQLModel):
    name: str = Field(index=True)


class Team(TeamBase, table=True):
    id: Optional[int] = Field(default=None, primary_key=True)

    members: list["Employee"] = Relationship(
        back_populates="teams", link_model=TeamMember
    )

    result_areas: list["ResultArea"] = Relationship(back_populates="team")
    member_links: list[TeamMember] = Relationship(back_populates="team")


class TeamCreate(TeamBase):
    pass


class TeamRead(TeamBase):
    id: int


class TeamUpdate(SQLModel):
    name: Optional[str]


class EmployeeBase(SQLModel):
    given_name: str = Field(index=True)
    additional_name: Optional[str] = Field(default=None)
    family_name: Optional[str] = Field(default=None)


class Employee(EmployeeBase, table=True):
    id: Optional[int] = Field(default=None, primary_key=True)

    teams: list[Team] = Relationship(back_populates="members", link_model=TeamMember)
    team_links: list[TeamMember] = Relationship(back_populates="employee")


class EmployeeRead(EmployeeBase):
    id: int


class EmployeeCreate(EmployeeBase):
    pass


class EmployeeUpdate(SQLModel):
    pass


class AvailabilityBase(SQLModel):
    available_at: datetime
    team_member_id: int | None = Field(default=None, foreign_key="teammember.id")


class Availability(AvailabilityBase):
    id: Optional[int] = Field(default=None, primary_key=True)

    # team_member: TeamMember = Relationship(back_populates="availability")


class AvailabilityCreate(AvailabilityBase):
    pass


class AvailabilityRead(AvailabilityBase):
    id: int


class ProcessBase(SQLModel):
    name: str = Field(index=True)
    code_name: str = Field(index=True)
    description: Optional[str]


class Process(ProcessBase, table=True):
    id: Optional[int] = Field(default=None, primary_key=True)

    result_areas: list["ResultArea"] = Relationship(back_populates="process")


class ProcessCreate(ProcessBase):
    pass


class ProcessRead(ProcessBase):
    id: int


class ProcessUpdate(SQLModel):
    name: Optional[str]
    code_name: Optional[str]
    description: Optional[str]


# https://schema.org/PlanAction
class AllocationBase(SQLModel):
    startTime: Optional[datetime]
    endTime: Optional[datetime]
    duration_minutes: Optional[int]

    number_of_employees: int = Field(default=1)


class Allocation(AllocationBase, table=True):
    id: Optional[int] = Field(default=None, primary_key=True)

    weekday: Optional[int] = Field(index=True)
    week: Optional[int] = Field(index=True)
    year: Optional[int] = Field(index=True)

    result_area_id: int | None = Field(default=None, foreign_key="resultarea.id")
    result_area: "ResultArea" = Relationship(back_populates="allocations")
    
    assigned_team_members: list[TeamMember] = Relationship(back_populates="assignments", link_model=Assignment)

class AllocationCreate(AllocationBase):
    pass


class AllocationRead(AllocationBase):
    id: int


class AllocationUpdate(SQLModel):
    duration_minutes: int | None
    startTime: datetime | None
    endTime: datetime | None
    number_of_employees: int | None


class ResultAreaBase(SQLModel):
    name: str
    code_name: str = Field(index=True, nullable=False)
    order: Optional[int] = Field(nullable=True)


class ResultArea(ResultAreaBase, table=True):
    __table_args__ = (UniqueConstraint("code_name", "order", name="order_code_name"),)
    id: Optional[int] = Field(default=None, primary_key=True)

    process_id: int | None = Field(default=None, foreign_key="process.id")
    team_id: int | None = Field(default=None, foreign_key="team.id")

    team: Team | None = Relationship(back_populates="result_areas")
    team_members: list[TeamMember] = Relationship(
        back_populates="result_areas", link_model=TeamMemberResultAreaLink
    )
    process: Process | None = Relationship(back_populates="result_areas")
    allocations: Allocation | None = Relationship(back_populates="result_area")


class ResultAreaCreate(ResultAreaBase):
    pass


class ResultAreaRead(ResultAreaBase):
    id: int


class ResultAreaReadWithoutOrder(ResultAreaBase):
    id: int
    order: int = Field(exclude=True)




#########################################################################################
# Responses
#########################################################################################
class EmployeeRespons(EmployeeRead):
    teams: list[TeamRead]


class ProcessResponse(ProcessRead):
    result_areas: list[ResultAreaBase] = []


class TeamListRespons(TeamRead):
    pass


class TeamRespons(TeamRead):
    members: list[EmployeeRead]


class TeamMemberResponse(TeamMemberRead):
    employee: EmployeeRead
    result_areas: list[ResultAreaReadWithoutOrder]


class AllocationResponse(AllocationRead):
    assignments: list[AssignmentRead]


class AssignmentResponse(AssignmentRead):
    pass
